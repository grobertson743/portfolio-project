from black import path_empty
from django.urls import path

from greg.views import (
    HomePageView,
    PortfolioPageView,
    ContactPageView,
    ResumePageView,
    SkillsPageView,
)

urlpatterns = [
    path("", HomePageView.as_view(), name="home"),
    path("portfolio/", PortfolioPageView.as_view(), name="portfolio"),
    path("resume/", ResumePageView.as_view(), name="resume"),
    path("skills/", SkillsPageView.as_view(), name="skills"),
    path("contact/", ContactPageView.as_view(), name="contact"),
]
