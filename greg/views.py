from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.urls import reverse_lazy

# Create your views here.


class HomePageView(TemplateView):

    template_name = "personal/home.html"
    success_url = reverse_lazy("")


class PortfolioPageView(TemplateView):

    template_name = "personal/portfolio.html"

class ContactPageView(TemplateView):

    template_name = "personal/contact.html"

class ResumePageView(TemplateView):

    template_name = "personal/resume.html"


class SkillsPageView(TemplateView):

    template_name = "personal/skills.html"